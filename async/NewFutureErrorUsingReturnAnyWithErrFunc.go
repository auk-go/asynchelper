package async

import (
	"context"

	"gitlab.com/auk-go/asynchelper/asyncfunc"
)

// NewFutureErrorUsingReturnAnyWithErrFunc
//
// executes the async function
//
// Reference : https://t.ly/hfxe
func NewFutureErrorUsingReturnAnyWithErrFunc(
	anyReturnFunc asyncfunc.AnyWithErrorReturnFunc,
) ValueWithErrorAwaiter {
	var result interface{}
	var err error

	c := make(chan struct{})

	go func() {
		defer close(c)

		result, err = anyReturnFunc()
	}()

	return futureWithError{
		await: func(ctx context.Context) (interface{}, error) {
			select {
			case <-ctx.Done():
				return nil, ctx.Err()
			case <-c:
				return result, err
			}
		},
	}
}
