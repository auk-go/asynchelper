package async

import (
	"sync"
)

type WorkerResponse struct {
	workerCount, tasksLength int
	waitGroup                *sync.WaitGroup
	guard                    chan struct{}
}

func EmptyWorkerResponse() *WorkerResponse {
	return nil
}

func NewWorkerResponseUsingResponse(
	workerCount int,
	guard chan struct{},
	response *Response,
) *WorkerResponse {
	if response == nil {
		return &WorkerResponse{
			workerCount: workerCount,
			tasksLength: 0,
			waitGroup:   nil,
			guard:       guard,
		}
	}

	return &WorkerResponse{
		workerCount: workerCount,
		tasksLength: response.tasksLength,
		waitGroup:   response.waitGroup,
		guard:       guard,
	}
}

func (it *WorkerResponse) IsNoWaitRequired() bool {
	return it == nil || it.workerCount == 0 || it.tasksLength <= 1 || it.waitGroup == nil
}

func (it *WorkerResponse) IsRequiresWait() bool {
	return !it.IsNoWaitRequired()
}

func (it *WorkerResponse) HasGuard() bool {
	return it != nil && it.guard != nil
}

func (it *WorkerResponse) Wait() {
	if it.IsNoWaitRequired() {
		return
	}

	it.waitGroup.Wait()

	if it.guard != nil {
		close(it.guard)
	}
}
