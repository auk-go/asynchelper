package async

import (
	"context"

	"gitlab.com/auk-go/asynchelper/asyncfunc"
)

// NewFuture
//
// executes the async function
//
// Reference : https://t.ly/hfxe
func NewFuture(anyReturnFunc asyncfunc.AnyReturnFunc) Awaiter {
	var result interface{}
	c := make(chan struct{})

	go func() {
		defer close(c)

		result = anyReturnFunc()
	}()

	return future{
		await: func(ctx context.Context) interface{} {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-c:
				return result
			}
		},
	}
}
