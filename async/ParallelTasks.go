package async

import (
	"gitlab.com/auk-go/asynchelper/asyncfunc"
)

// ParallelTasks
//
// no waiting on any tasks
func ParallelTasks(tasks ...asyncfunc.VoidTask) {
	length := len(tasks)
	if length == 0 {
		return
	}

	for i := 0; i < length; i++ {
		go tasks[i]()
	}
}
