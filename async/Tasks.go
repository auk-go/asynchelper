package async

import (
	"sync"

	"gitlab.com/auk-go/asynchelper/asyncfunc"
	"gitlab.com/auk-go/core/constants"
)

// Tasks
//
// Wait group is NOT called with wait.
// So developer needs to call it after when needs to be waited.
//
// Must call waitGroup.Wait() before working with data.
func Tasks(
	tasks ...asyncfunc.VoidTask,
) *sync.WaitGroup {
	return TasksPlusWaitCount(
		constants.Zero,
		tasks...)
}
