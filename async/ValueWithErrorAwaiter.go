package async

// ValueWithErrorAwaiter interface has the method signature for await
type ValueWithErrorAwaiter interface {
	Await() (interface{}, error)
}
