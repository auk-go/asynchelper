package main

import (
	"fmt"
	"time"

	"gitlab.com/auk-go/asynchelper/async"
)

func testAsyncWorkerFunc() {
	now := time.Now()
	s := []string{"hello", "world", "again"}
	resp := async.Worker.IndexFuncProcessorDefault(
		len(s),
		func(index int) {
			time.Sleep(1 * time.Second)
			fmt.Println(s[index])
		},
	)

	resp.Wait()

	// Should complete in 1 sec
	fmt.Println("elapsed:", time.Since(now))
}
