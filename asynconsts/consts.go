package asynconsts

const (
	NonAsyncSafeRangeSmall  = 15
	NonAsyncSafeRangeMedium = 50
	NonAsyncSafeRangeLarge  = 100
	SupportedMillisecond    = 500
)
