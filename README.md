<!-- ![Use Package logo](UseLogo) -->

# AsyncHelper Intro

helper function for dealing with async running codes.

## Git Clone

`git clone https://gitlab.com/auk-go/asynchelper.git`

### 2FA enabled, for linux

`git clone https://[YourGitLabUserName]:[YourGitlabAcessTokenGenerateFromGitlabsTokens]@gitlab.com/auk-go/asynchelper.git`

### Prerequisites

- Update git to latest 2.29
- Update or install the latest of Go 1.15.2
- Either add your ssh key to your gitlab account
- Or, use your access token to clone it.

## Installation

`go get gitlab.com/auk-go/asynchelper`

### Go get issue for private package

- Update git to 2.29
- Enable go modules. (Windows : `go env -w GO111MODULE=on`, Unix : `export GO111MODULE=on`)
- Add `gitlab.com/auk-go` to go env private

To set for Windows:

`go env -w GOPRIVATE=[AddExistingOnes;]gitlab.com/auk-go`

To set for Unix:

`expoort GOPRIVATE=[AddExistingOnes;]gitlab.com/auk-go`

To go get as root:

- [Linux, Go Get Fix with SSH GitLabs](https://gitlab.com/auk-go/os-manuals/-/issues/43)

## Why AsyncHelper?

Provides and reduces code lines for dealing with async stuff.

## Examples

`Code Smaples`

## Acknowledgement

Any other packages used

## Links

## Issues

- [Create your issues](https://gitlab.com/auk-go/asynchelper/-/issues)

## Notes

## Contributors

## License

[Evatix MIT License](/LICENSE)
