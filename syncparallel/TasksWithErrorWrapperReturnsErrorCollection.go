package syncparallel

import (
	"sync"

	"gitlab.com/auk-go/errorwrapper/errfunc"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

func TasksWithErrorWrapperReturnsErrorCollection(
	isContinueOnError bool,
	tasks ...errfunc.WrapperFunc,
) *errwrappers.Collection {
	length := len(tasks)
	if length == 0 {
		return nil
	}

	mutexErrorCollection := errwrappers.MutexEmpty()

	if length == 1 {
		errWrapper := tasks[0]()
		mutexErrorCollection.AddWrapperPtr(errWrapper)

		return mutexErrorCollection.Collection()
	}

	isErrorFound := false
	wg := sync.WaitGroup{}

	runWrapper := func(index int) {
		defer wg.Done()

		if !isContinueOnError && isErrorFound {
			return
		}

		errWrapper := tasks[index]()

		if errWrapper.HasError() {
			mutexErrorCollection.AddWrapperPtr(errWrapper)
			isErrorFound = true
		}
	}

	for i := 0; i < length; i++ {
		if !isContinueOnError && isErrorFound {
			break
		}

		wg.Add(1)
		go runWrapper(i)

		if !isContinueOnError && isErrorFound {
			break
		}
	}

	wg.Wait()

	if mutexErrorCollection.HasError() {
		return mutexErrorCollection.Collection()
	}

	go mutexErrorCollection.Dispose()

	return nil
}
