package syncparallel

import (
	"gitlab.com/auk-go/errorwrapper/errfunc"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

func TasksWithErrorWrapper(
	isContinueOnError bool,
	tasks ...errfunc.WrapperFunc,
) *errwrappers.Collection {
	length := len(tasks)
	if length == 0 {
		return nil
	}

	if length == 1 {
		errCollection := errwrappers.Empty()
		errCollection.AddFunction(tasks[0])

		return errCollection
	}

	errorCollection := TasksWithErrorWrapperReturnsErrorCollection(
		isContinueOnError,
		tasks...)

	if errorCollection.HasError() {
		return errorCollection
	}

	return nil
}
