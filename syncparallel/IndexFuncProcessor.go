package syncparallel

import (
	"sync"

	"gitlab.com/auk-go/asynchelper/asyncfunc"
)

// IndexFuncProcessor
//
// Wait on all tasks to be finished
func IndexFuncProcessor(
	length int,
	processorFunc asyncfunc.IndexProcessorFunc,
) {
	if length == 0 {
		return
	}

	if length == 1 {
		processorFunc(0)

		return
	}

	wg := sync.WaitGroup{}

	runWrapper := func(index int) {
		processorFunc(index)

		wg.Done()
	}

	wg.Add(length)

	for i := 0; i < length; i++ {
		go runWrapper(i)
	}

	wg.Wait()
}
